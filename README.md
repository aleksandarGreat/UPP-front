# Business Process Management - Front-end

University project mainly focused on successful implementation of process-driven development.
Works with .bpmn files with Spring Boot as support to creating a server side.

## Basic functionallity

Users need to register to an application in order to use it.
Predefined user for testing and general organization is user: admin, password: admin with ADMIN rights.
Users can have different types that limit their actions.

After the user has been registered sucessfully and has accepted an email invitation, he will be able to access the site.
He can choose to create new offers and accept, decline, propose and close the existing one.

Agents can post new job offerings, witch new users can choose to accept and give delivery date proposals.

All processes are finished once both sides have confirmed that job was completed and gave each other proper rating.

## Server side

This project represents only front-end side of the overall project, server side can be found here:
https://gitlab.com/aleksandarGreat/UPP

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README]
(https://github.com/angular/angular-cli/blob/master/README.md).
