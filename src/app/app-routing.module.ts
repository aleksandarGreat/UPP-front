import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';

import { TasksComponent } from './tasks/tasks.component';
import { RecipesComponent } from './recipes/recipes.component';
import { TaskDetailComponent } from './tasks/task-detail/task-detail.component';
//import {SignupComponent} from "./auth/signup/signup.component";
//import {SigninComponent} from "./auth/signin/signin.component";

const appRoutes: Routes = [
	//{ path:'', redirectTo:'/tasks', pathMatch:'full'},
	{ path:'tasks', component: TasksComponent, children:[
		//{path:'', component: RecipeStartComponent},
		//{path:'new', component: RecipeEditComponent,canActivate: [AuthGuard]},
		{path:':id', component: TaskDetailComponent},
		//{path:':id/edit', component: RecipeEditComponent,canActivate: [AuthGuard]}
	]},
  	{ path: 'login', component: LoginComponent},
  //{ path: 'signin', component: SigninComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}