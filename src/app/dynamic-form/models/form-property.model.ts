
export class FormProperty{

	public type:string;
    public name:string;
    public label:string;
    public placeholder:string;
    public required:boolean;
    public writable:boolean;
    public readable:boolean;
    public values:string[];


	constructor(type:string, name:string, label:string, placeholder:string, 
				required:boolean, writable:boolean, readable:boolean , values:string[]){
		this.type = type;
		this.name = name;
		this.label = label;
		this.placeholder = placeholder;
		this.required = required;
		this.writable = writable;
		this.readable = readable;
		this.values = values;
	}
	
}