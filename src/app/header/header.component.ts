import  {Component} from '@angular/core';
import { Response} from '@angular/http';
import { Router } from '@angular/router';
import  { ServerService } from '../server.service';


@Component({
	selector : 'app-header',
	templateUrl: './header.component.html'
})
export class HeaderComponent {

  constructor(private serverService: ServerService, private router: Router) {}

  onSaveData(){

  }

  onFetchData(){
  }

  onLogout(){
    this.serverService.tempUser = null;
    this.serverService.isLogged = false;
    this.router.navigateByUrl('/login');
  }

  onStartRegistration() {
  	this.serverService.startProcessRegistration().subscribe(
      (response: Response) => {
        console.log(response);
        this.serverService.tempUser = response.json().tempUser;
        console.log(this.serverService.tempUser);
        this.router.navigateByUrl('/tasks');
      }
    );
  }

  onStartMainProcess(){
    this.serverService.startProcessTender().subscribe(
      (response: Response) => {
        console.log(response.json());
        this.router.navigateByUrl('/tasks');
      }
    );
  }

}
