export class StartProcessModel {

	public tempUser: string;
	public processId: string;

	constructor(tempUser: string, processId: string){
		this.tempUser = tempUser;
		this.processId = processId;
	}

}
