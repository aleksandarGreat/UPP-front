import {Component} from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { ServerService } from '../server.service';
import { LoginModel } from './login.model';
import { Router } from '@angular/router';

@Component({
	selector : 'app-login',
	templateUrl: './login.component.html'
})
export class LoginComponent{

  username: string;
  password: string;

  constructor(private serverService: ServerService, private router: Router) {}

  onSaveData(){

  }

  onFetchData(){

  }

  onLogout(){

  }

  onSubmit() {
    console.log("username: " + this.username + " password: " + this.password);
    this.serverService.login(new LoginModel(this.username, this.password)).subscribe(
           (response: Response) => {
              let loginModel: LoginModel = response.json();
              this.serverService.isLogged = loginModel.logStatus;
              if (loginModel.logStatus) {
                this.serverService.tempUser = loginModel.username;
                toastr.info("Login successfully finished!");
                this.router.navigateByUrl('');
              } else {
                this.serverService.tempUser = null;
              }
           }
       );
  }

}
