
export class LoginModel{

	public username: string;
    public password:string;
    public logStatus:boolean;

	constructor(username:string, password:string){
		this.username = username;
		this.password = password;
	}

}
