import { Injectable } from '@angular/core'
import { Recipe } from './recipe.model';
import {Subject} from "rxjs/Subject";


@Injectable()
export class RecipeService{
  recipeChanged = new Subject<Recipe[]>();

	private recipes: Recipe[] = [
    new Recipe(
      'Tasty Schnitzel',
      'A super-tasty Schnitzel - just awesome!',
      'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG'),
    new Recipe('Big Fat Burger',
      'What else you need to say?',
      'https://upload.wikimedia.org/wikipedia/commons/b/be/Burger_King_Angus_Bacon_%26_Cheese_Steak_Burger.jpg')
  	];

  	constructor(){}

  	setRecipes(recipes:Recipe[]){
  	  this.recipes = recipes;
  	  this.recipeChanged.next(this.recipes.slice());
    }

  	getRecipes(){
  		return this.recipes.slice();
  	}

    getRecipe(index:number){
      return this.recipes[index];
    }

 

  	addRecipe(recipe: Recipe){
  	   this.recipes.push(recipe);
  	   this.recipeChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe){
  	  this.recipes[index] = newRecipe;
      this.recipeChanged.next(this.recipes.slice())
    }

    deleteRecipe(index: number){
      this.recipes.splice(index, 1);
      this.recipeChanged.next(this.recipes.slice());
    }
}
