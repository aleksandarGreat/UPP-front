import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { TaskModel } from './tasks/task.model';
import { MyFormPropertyModel } from './tasks/task-detail/my-form-property.model';
import { LoginModel } from './login/login.model';


@Injectable()
export class ServerService {

  public tempUser: string;
  public isLogged: boolean;

  private  urlUPPBackend = 'http://localhost:8080/api';

  constructor(private http: Http) {

  }

  postFormOnTask(taskId:string, formProperties: MyFormPropertyModel[]) {
    return this.http.post(this.urlUPPBackend + "/tasks/form/" + taskId, formProperties);
  }


  startProcessRegistration() {
    return this.http.get(this.urlUPPBackend + "/process/registration");
  }

  startProcessTender() {
    return this.http.post(this.urlUPPBackend + "/process/tender", this.tempUser);
  }

  getUserTasks() {
    return this.http.get(this.urlUPPBackend + "/tasks/" + this.tempUser);
  }

  getFormPropetries(taskId: string) {
    return this.http.get(this.urlUPPBackend + "/tasks/form/" + taskId);
  }


  login(loginJson:LoginModel){
    return this.http.post(this.urlUPPBackend + "/login", loginJson);
  }

}
