export class EnumItemModel{

	public name?:string;
    public value?:string;

	constructor(name:string, value:string){
		this.value = value;
		this.name = name;
	}

}
