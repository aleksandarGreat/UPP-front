import { EnumItemModel } from './enum-item.model';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import {OnInit} from '@angular/core';

export class MyFormPropertyModel implements OnInit {
  public type?: string;
    public name?: string;
    public label?: string;
    public value?: string;
    public required?: boolean;
    public writable?: boolean;
    public values?: EnumItemModel[];
    public multi: string[];

    constructor(type: string, name: string, label: string, value: string,
                writable: boolean, required: boolean , values: EnumItemModel[], multi: string[]) {
      this.type = type;
      this.name = name;
      this.label = label;
      this.value = value;
      this.required = required;
      this.writable = writable;
      this.values = values;
      this.multi = multi;
    }

    ngOnInit() {
    }

    onChange() {
      for (const multi of this.values) {
        this.value += ';' + multi.name;
      }
    }

}
