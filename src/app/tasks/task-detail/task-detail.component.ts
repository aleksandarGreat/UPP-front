import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TaskModel } from '../task.model';
import { ServerService } from '../../server.service';
import { Response } from '@angular/http';
import { MyFormPropertyModel } from './my-form-property.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {
  task: TaskModel;
  id: string;
  formProperties: MyFormPropertyModel[];

  constructor(private route: ActivatedRoute, private router: Router, private serverService: ServerService) { }

  ngOnInit() {
      this.route.params.subscribe(
        (params: Params) => {
           this.id = params['id'];
           console.log(this.id);
           this.serverService.getFormPropetries(this.id).subscribe(
              (response: Response) => {
                console.log(response);
                this.formProperties = response.json();
              }
          );
      });
  }

  onSubmit() {
    console.log(this.formProperties);
    this.serverService.postFormOnTask(this.id, this.formProperties)
      .subscribe(
           (response: Response) => {
              console.log(response);
              toastr.info("Uspesno izvrsen task");
              this.router.navigateByUrl('/tasks');
           }
       );
  }


}
