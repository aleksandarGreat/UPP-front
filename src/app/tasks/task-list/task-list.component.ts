import { Component, OnDestroy, OnInit } from '@angular/core';
import { ServerService } from '../../server.service' ;
import { Response} from '@angular/http';
import { TaskModel } from '../task.model';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy{
  subscription: Subscription;
  tasks: TaskModel[];

  constructor(private serverService: ServerService) { }

  ngOnInit() {
    let parsedStrings: string[] = window.location.href.split('=');
     if (parsedStrings.length > 1) {
       this.serverService.tempUser = parsedStrings[1];
     }
    this.tasks = [];
    let x = this;
    window.setInterval(function(){
      console.log(x.serverService.tempUser);
      if(x.serverService.tempUser != undefined){
        x.serverService.getUserTasks().subscribe(
            (response: Response) => {
              console.log(response.json());
              let responseTasks = response.json();
              x.tasks.splice(0, x.tasks.length);
              for (var i = responseTasks.length - 1; i >= 0; i--) {
                x.tasks.push(new TaskModel(responseTasks[i].name, responseTasks[i].id, 'https://cdn4.iconfinder.com/data/icons/free-large-boss-icon-set/128/Supervisor.png'))
              }
            }
        );
      }
    }, 5000);
  }

  onNewRecipe() {

  }

  ngOnDestroy(){
    //this.subscription.unsubscribe();
  }

}
